package org.psf.relief.controller;

import org.psf.relief.model.TransactionRequest;
import org.psf.relief.model.TransactionResponse;
import org.psf.relief.model.UserRequest;
import org.psf.relief.model.UserResponse;
import org.psf.relief.service.ReliefService;
import org.psf.relief.util.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(ReliefController.API.RELIEF)
@RequiredArgsConstructor
@Validated
public class ReliefController {

	@NoArgsConstructor(access = AccessLevel.PRIVATE)
	public static final class API {
		public static final String RELIEF = "relief";
		public static final String ADD_DONOR = "addDonor";
		public static final String ADD_BENEFICIARY = "addBeneficiary";
		public static final String DONATE = "donate";
		public static final String RECEIVE = "receive";
	}

	@Autowired
	private final ReliefService service;

	@PostMapping(ReliefController.API.ADD_BENEFICIARY)
	@ResponseStatus(HttpStatus.OK)
	public UserResponse addBeneficiary(@RequestBody UserRequest request) {
		return service.addUser(request, Constant.ENTITY_BENEFICIARY);
	}

	@PostMapping(ReliefController.API.ADD_DONOR)
	@ResponseStatus(HttpStatus.OK)
	public UserResponse addDonor(@RequestBody UserRequest request) {
		return service.addUser(request, Constant.ENTITY_DONOR);
	}

	@PostMapping(ReliefController.API.DONATE)
	@ResponseStatus(HttpStatus.OK)
	public TransactionResponse donate(@RequestBody TransactionRequest request) {
		return service.transact(request, Constant.ENTITY_DONOR);
	}

	@PostMapping(ReliefController.API.RECEIVE)
	@ResponseStatus(HttpStatus.OK)
	public TransactionResponse receive(@RequestBody TransactionRequest request) {
		return service.transact(request, Constant.ENTITY_BENEFICIARY);
	}

}