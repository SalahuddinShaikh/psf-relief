package org.psf.relief.dto;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Transaction extends Auditable<String> {
	@Id
	@GeneratedValue
	private Long id;

	private String transactionId;

	@OneToOne(cascade = { CascadeType.DETACH, CascadeType.PERSIST })
	private User donor;

	@OneToOne(cascade = { CascadeType.DETACH, CascadeType.PERSIST })
	private User beneficiary;

	private Double amount;

	private String goods;

	private Boolean pledged;
	
	private Integer noOfPeople;
}
