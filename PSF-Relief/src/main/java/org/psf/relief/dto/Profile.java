package org.psf.relief.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Profile extends Auditable<String> {
	@Id
	@GeneratedValue
	private Long id;

	private String name;

	private String mobile;

	private String email;

	@ManyToOne
	private Address address;

}
