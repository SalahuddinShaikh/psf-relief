package org.psf.relief.model;

import lombok.Builder;
import lombok.Getter;

@Getter
public class TransactionResponse extends APIResponse {
	private String transactionId;

	@Builder
	public TransactionResponse(boolean success, String msg, String transactionId) {
		super(success, msg);
		this.transactionId = transactionId;
	}

}
