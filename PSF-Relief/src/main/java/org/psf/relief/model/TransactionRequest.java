package org.psf.relief.model;

import javax.validation.constraints.Positive;

import lombok.Data;

@Data
public class TransactionRequest {

	// for user lookup
	@Positive
	private String userModileNumber;

	// beneficiary
	@Positive
	private Integer noOfPeople;

	// donor
	private Boolean pledge;

	@Positive
	private Double amount;

	private String goods;

}