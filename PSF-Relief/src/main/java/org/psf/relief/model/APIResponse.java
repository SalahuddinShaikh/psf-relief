package org.psf.relief.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class APIResponse {
	private boolean success;
	private String msg;

}
