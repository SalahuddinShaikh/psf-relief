package org.psf.relief.model;

import lombok.Builder;
import lombok.Getter;

@Getter
public class UserResponse extends APIResponse {
	private long userId;

	@Builder
	public UserResponse(boolean success, String msg, long userId) {
		super(success, msg);
		this.userId = userId;
	}

}
