package org.psf.relief.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

import lombok.Data;

@Data
public class UserRequest {

	@NotEmpty
	private String name;

	@Pattern(regexp = "(^$|[0-9]{10})")
	private String mobile;

	@Email
	private String email;

	private String line1;
	private String line2;
	private String city;
	private String state;

	@NotEmpty
	private String pin;

}
