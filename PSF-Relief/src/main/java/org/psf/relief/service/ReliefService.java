package org.psf.relief.service;

import org.psf.relief.model.TransactionRequest;
import org.psf.relief.model.TransactionResponse;
import org.psf.relief.model.UserRequest;
import org.psf.relief.model.UserResponse;

public interface ReliefService {

	UserResponse addUser(UserRequest request, String entity);

	TransactionResponse transact(TransactionRequest request, String entity);

}
