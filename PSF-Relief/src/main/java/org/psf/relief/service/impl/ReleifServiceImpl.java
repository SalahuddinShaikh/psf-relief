package org.psf.relief.service.impl;

import java.util.Optional;
import java.util.UUID;

import org.psf.relief.dto.Address;
import org.psf.relief.dto.Profile;
import org.psf.relief.dto.Role;
import org.psf.relief.dto.Transaction;
import org.psf.relief.dto.Transaction.TransactionBuilder;
import org.psf.relief.dto.User;
import org.psf.relief.model.TransactionRequest;
import org.psf.relief.model.TransactionResponse;
import org.psf.relief.model.UserRequest;
import org.psf.relief.model.UserResponse;
import org.psf.relief.repository.AddressRepository;
import org.psf.relief.repository.ProfileRepository;
import org.psf.relief.repository.RoleRepository;
import org.psf.relief.repository.TransactionRepository;
import org.psf.relief.repository.UserRepository;
import org.psf.relief.service.ReliefService;
import org.psf.relief.util.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ReleifServiceImpl implements ReliefService {

	@Autowired
	private final RoleRepository roleRepository;

	@Autowired
	private final UserRepository userRepository;

	@Autowired
	private final ProfileRepository profileRepository;

	@Autowired
	private final AddressRepository addressRepository;

	@Autowired
	private TransactionRepository transactionRepository;

	@Override
	@Transactional
	public UserResponse addUser(UserRequest request, String entity) {
		Role role = getRole(entity);
		User user = userRepository.save(getUser(request, role));
		return getResonse(true, "user is created", user.getId());
	}

	private Address getAddress(UserRequest request) {
		return Address.builder().city(request.getCity()).line1(request.getLine1()).line2(request.getLine2())
				.pin(request.getPin()).state(request.getState()).build();
	}

	private Profile getProfile(UserRequest request, Address address) {
		return Profile.builder().address(address).email(request.getEmail()).mobile(request.getMobile())
				.name(request.getName()).build();
	}

	private UserResponse getResonse(boolean b, String string, Long id) {
		return UserResponse.builder().msg(string).success(b).userId(id).build();
	}

	private TransactionResponse getResonse(boolean b, String string, String id) {
		return TransactionResponse.builder().msg(string).success(b).transactionId(id).build();
	}

	private Role getRole(String entityDonor) {
		return roleRepository.getOrCreate(Role.builder().name(entityDonor).build());
	}

	private User getUser(Role role, Profile profile) {
		return User.builder().profile(profile).role(role).build();
	}

	private User getUser(UserRequest request, Role role) {
		Address address = addressRepository.getOrCreate(getAddress(request));
		Profile profile = profileRepository.getOrCreate(getProfile(request, address));
		return userRepository.getOrCreate(getUser(role, profile));
	}

	@Override
	@Transactional
	public TransactionResponse transact(TransactionRequest request, String entity) {
		Optional<User> optional = userRepository.findByProfile_mobile(request.getUserModileNumber());
		TransactionBuilder builder = Transaction.builder().transactionId(UUID.randomUUID().toString());
		if (optional.isPresent()) {
			User user = optional.get();
			if (Constant.ENTITY_BENEFICIARY.equals(entity)) {
				builder.amount(request.getAmount()).goods(request.getGoods()).pledged(request.getPledge())
						.beneficiary(user);
			} else {
				builder.noOfPeople(request.getNoOfPeople()).donor(user);
			}
			Transaction transaction = transactionRepository.save(builder.build());
			return getResonse(true, "Transaction successfull", transaction.getTransactionId());
		} else {
			return getResonse(false, "No user found associated with this mobile number", request.getUserModileNumber());
		}
	}

}
