package org.psf.relief.advice;

import org.hibernate.exception.ConstraintViolationException;
import org.psf.relief.model.APIResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(value = ConstraintViolationException.class)
	protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
		APIResponse response = new APIResponse(false, ex.getMessage());
		return handleExceptionInternal(ex, response, new HttpHeaders(), HttpStatus.CONFLICT, request);
	}
}