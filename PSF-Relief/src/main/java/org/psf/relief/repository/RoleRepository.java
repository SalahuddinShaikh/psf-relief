package org.psf.relief.repository;

import org.psf.relief.dto.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long>, ExtendedJpaRepository<Role, Long> {

}
