package org.psf.relief.repository;

import org.psf.relief.dto.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfileRepository extends JpaRepository<Profile, Long>, ExtendedJpaRepository<Profile, Long> {

}
