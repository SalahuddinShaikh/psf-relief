package org.psf.relief.repository;

import org.psf.relief.dto.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long>, ExtendedJpaRepository<Address, Long> {

}
