package org.psf.relief.repository;

import org.psf.relief.dto.Transaction;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionRepository extends ExtendedJpaRepository<Transaction, Long> {

}
