package org.psf.relief.repository;

import java.util.Optional;

import org.psf.relief.dto.Profile;
import org.psf.relief.dto.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long>, ExtendedJpaRepository<User, Long> {

	Optional<User> findByProfile(Profile requestedProfile);

	Optional<User> findByProfile_mobile(String mobile);

}
