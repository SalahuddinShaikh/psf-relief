package org.psf.relief.repository;

import java.io.Serializable;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

public class ExtendedJpaRepositoryImpl<T, ID extends Serializable> extends SimpleJpaRepository<T, ID>
		implements ExtendedJpaRepository<T, ID> {

	public ExtendedJpaRepositoryImpl(JpaEntityInformation<T, ID> domainClass, EntityManager em) {
		super(domainClass, em);
	}

	@Override
	public T getOrCreate(T prob) {
		Optional<T> findOne = findOne(Example.of(prob));
		if (findOne.isPresent())
			return findOne.get();
		return save(prob);
	}
}