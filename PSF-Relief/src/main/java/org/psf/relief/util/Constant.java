package org.psf.relief.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Constant {
	public static final String DONATION_TYPE_NONE = "none";
	public static final String DONATION_TYPE_MIXED = "mixed";
	public static final String DONATION_TYPE_GOODS = "goods";
	public static final String DONATION_TYPE_MONEY = "money";
	public static final String ENTITY_BENEFICIARY = "beneficiary";
	public static final String ENTITY_DONOR = "donor";

}
