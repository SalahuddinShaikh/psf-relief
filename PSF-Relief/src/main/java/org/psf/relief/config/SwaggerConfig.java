package org.psf.relief.config;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("org.psf.relief.controller")).build().apiInfo(apiInfo())
				.produces(getSet(MediaType.APPLICATION_JSON)).consumes(getSet(MediaType.APPLICATION_JSON));
	}

	private ApiInfo apiInfo() {
		return new ApiInfo("PSF Relief API", "PSF Relief API", "API TOS", "Terms of service",
				new Contact("PSF Relief Team", "", ""), "License of API", "API license URL", Collections.emptyList());

	}

	private Set<String> getSet(MediaType applicationJson) {
		return new HashSet<>(Arrays.asList(applicationJson.toString()));
	}
}